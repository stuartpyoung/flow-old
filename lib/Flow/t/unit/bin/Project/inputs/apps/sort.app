---
appname: sort
description: ''
location: samtools
parameters:
  -
    ordinal: 1
    value: sort
    valuetype: string
  -
    ordinal: '2'
    value: $normal
    valuetype: string
  -
    ordinal: '3'
    value: $cur_dir/${sample}_N_sorted
    valuetype: directory
